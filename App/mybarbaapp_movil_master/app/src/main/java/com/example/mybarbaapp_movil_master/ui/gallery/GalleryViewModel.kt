package com.example.mybarbaapp_movil_master.ui.gallery

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class GalleryViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "PROMOCIONES"
    }
    val text: LiveData<String> = _text
}