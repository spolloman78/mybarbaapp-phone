package com.example.mybarbaapp_movil_master.ui.citas


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.mybarbaapp_movil_master.CitasHistorial
import com.example.mybarbaapp_movil_master.R

class CitasFragment : Fragment() {

    private lateinit var citasViewModel: CitasViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        citasViewModel =
            ViewModelProvider(this).get(CitasViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_citasm, container, false)

        citasViewModel.text.observe(viewLifecycleOwner, Observer {

        })
        return root

    }
}