package com.example.mybarbaapp_movil_master

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.mybarbaapp_movil_master.Clases.AdaptadorB
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class AgendarCitaActivity : AppCompatActivity() {
    private lateinit var JSONAgendar:JSONArray
    private var idUsuario:Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agendar_cita)
        val mPickTimeBtn = findViewById<EditText>(R.id.date)
        val mtime = findViewById<EditText>(R.id.timeP)
        val agendar = findViewById<Button>(R.id.agendar)
        val recy = findViewById<RecyclerView>(R.id.ServiciosAgendar)
        val json2=intent.extras?.getString("json","{}")
        var json= JSONArray(json2)
        var objetod=json.getJSONObject(0)
        Log.i("Maru",json.toString())
        idUsuario= intent.extras?.getInt("id",0)!!
        leerCitas(this,recy,json)
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        mPickTimeBtn.setOnClickListener {
        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // Display Selected date in TextView
            val xd:String
            val dia:String
            if(dayOfMonth<10){
                xd ="0"+dayOfMonth
            }else{
                xd=""+dayOfMonth
            }
            if(monthOfYear<10){
                dia ="0"+(monthOfYear+1)
            }else{
                dia=""+(monthOfYear+1)
            }
            mPickTimeBtn.setText("" + year + "/" + dia + "/" + xd) }, year, month, day)
            dpd.show()
            c.add(Calendar.YEAR, +1)
            c.add(Calendar.DAY_OF_WEEK_IN_MONTH, -1)
            dpd.datePicker.maxDate = c.timeInMillis }
//timepicker configuración
        mtime.setOnClickListener{
        val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            if(hour>8 && hour<21){
            c.set(Calendar.HOUR_OF_DAY, hour)
            c.set(Calendar.MINUTE, minute)
           mtime.setText(" "+ SimpleDateFormat("HH:mm").format(c.time))}
            else{
                "SELECCIONA UNA BUENA HORA".toast(this)
            }
        }
        TimePickerDialog(this, timeSetListener, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true).show()
        }
        agendar.setOnClickListener{
            if(mPickTimeBtn.text.length == 0|| mtime?.text.toString().length == 0){
                "Favor de llenar los campos faltantes".toast(this)
            }
            else{

                val fecha=mPickTimeBtn.text
                val hora=mtime.text
                agendar(fecha.toString(),hora.toString(),objetod,idUsuario,this)
            }
        }

    }

    fun leerCitas(context:Context,recyclerView:RecyclerView,jsonRespuesta:JSONArray){
        val pasada=recyclerView
        val tipo=0

        val manager = GridLayoutManager(context, 1)
        pasada.adapter= object :AdaptadorB(context, jsonRespuesta, R.layout.row_serviciobarbero,tipo){
            override fun seleccionar(lista:JSONArray) {
                Log.i("listaResultante",lista.toString())
                JSONAgendar=lista
            }

        }


        pasada?.layoutManager = manager


        }
        fun agendar(fecha:String, hora: String,jsonObject: JSONObject,id:Int,context: Context){
           // for (n in 0 until JSONAgendar.length()){
                val json=JSONAgendar.getJSONObject(0)
                json.put("fechas",fecha)
                json.put("hora",hora)
                json.put("idCliente",id)
                json.put("idServicio",jsonObject.get("id").toString().toInt())
                json.put("tiempo",jsonObject.get("tiempo").toString())
           // }
            Log.i("JSONRESULTANTE",JSONAgendar.toString()+" ")
            Log.i("nuevo", "Request para registrar Cita ")
            val queue = Volley.newRequestQueue(context)
            val url = "http://192.168.2.35:3000/citas/agendar"
            val jsonObjectRequest= JsonObjectRequest(Request.Method.POST,url,json,
                Response.Listener { response ->
                    Log.i("nuevo","Response ir $response")
                    if(response.length()>0){
                    if(response.get("tipo").toString()=="2"){
                        response.get("re").toString().toast(context)
                        val intent = Intent(context,MainActivity::class.java)
                        intent.putExtra("id",id)
                        startActivity(intent)
                        finish()
                    }else{
                        response.get("re").toString().toast(context)
                    }}


                }, Response.ErrorListener { error ->
                    error.printStackTrace()
                    Log.i("nuevo","Error bro al agregar el usuario")
                    "Hay un Error con la Conexíon al servidor, favor de checarlo".toast(context)
                }
            )
            queue.add(jsonObjectRequest)


        }
    fun String.toast(c:Context){
        Toast.makeText(c,this, Toast.LENGTH_LONG).show()
    }


    }

