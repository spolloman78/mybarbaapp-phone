package com.example.mybarbaapp_movil_master

import android.app.Notification
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray

class MyReceiver : BroadcastReceiver() {
    private val pendingIntent: PendingIntent? = null
    private val CHANNEL_ID = "NOTIFICACION"
    private val NOTIFICACION_ID = 0
    private var id:Int = 0

    override fun onReceive(context: Context, intent: Intent) {
        //Tu lógica de negocio irá aquí. En caso de requerir más de unos milisegundos, deberías usar un servicio
        id= intent.extras?.getInt("id",0)!!
        Log.i("hola id",id.toString())
        leerNotificaciones(context,id)

    }
    fun leerNotificaciones(context: Context,id:Int){
        Log.i("nuevo", "Request para Consultar Citas futuras"+id)
        val queue = Volley.newRequestQueue(context)
        val url = "http://192.168.2.35:3000/citas/Cercanas/"+id
        val json = JSONArray()
        val jsonObjectRequest = JsonArrayRequest(
            Request.Method.GET, url, json,
            Response.Listener { response ->
                Log.i("CitasProx", "Response ir $response")
                if(response.length()>0)
                {
                    val json4 = response.getJSONObject(0)
                    var encabezado=""+json4.get("descripcion").toString()
                    var texto=""+json4.get("fecha").toString()+" a las "+json4.get("horaInicio").toString()
                    createNotification(context,texto,encabezado)
                }

            }, Response.ErrorListener { error ->
                error.printStackTrace()
                Log.i("nuevo", "Error bro")
            })
        queue.add(jsonObjectRequest)
    }

    private fun createNotification(applicationContext: Context, texto:String, encabezado:String) {
        val builder = NotificationCompat.Builder(applicationContext, CHANNEL_ID)
        builder.setSmallIcon(R.mipmap.logoapp)
        builder.setContentTitle("Próxima Cita "+encabezado)
        builder.setContentText("Tienes una Cita: \n"+texto)
        builder.color = Color.rgb(46,54,78)
        builder.priority = NotificationCompat.PRIORITY_DEFAULT
        builder.setLights(Color.MAGENTA, 1000, 1000)
        builder.setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
        builder.setDefaults(Notification.DEFAULT_SOUND)
        builder.setLargeIcon(
            BitmapFactory.decodeResource(applicationContext.getResources(),
                R.mipmap.logoapp))
        builder.setStyle(
            NotificationCompat.BigPictureStyle()
            .bigPicture(
                BitmapFactory.decodeResource(applicationContext.getResources(),
                R.mipmap.the_cave_logo))
            .bigLargeIcon( BitmapFactory.decodeResource(applicationContext.getResources(),
                R.mipmap.the_cave_logo)))

        val intent = Intent(applicationContext, CitasHistorial::class.java)
        intent.putExtra("id",id)
        val pendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, 0)
        builder.setContentIntent(pendingIntent)
        val notificationManagerCompat = NotificationManagerCompat.from(applicationContext)
        notificationManagerCompat.notify(NOTIFICACION_ID, builder.build())


    }





}