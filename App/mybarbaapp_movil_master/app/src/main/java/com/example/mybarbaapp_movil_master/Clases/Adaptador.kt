package com.example.mybarbaapp_movil_master.Clases

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.mybarbaapp_movil_master.R
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.time.LocalDate

abstract class Adaptador(val c: Context?, val lista: JSONArray, val res:Int, val tipo:Int):
    RecyclerView.Adapter<Adaptador.MiVH>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adaptador.MiVH {
        val view = LayoutInflater.from(c).inflate(res, null)
        return MiVH(view)
    }

    override fun getItemCount(): Int {
        return lista.length()
    }

    override fun onBindViewHolder(holder: Adaptador.MiVH, position: Int) {
        val json4 = lista.getJSONObject(position)
        holder.bind(json4)
    }
    inner class MiVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(pelicula: JSONObject) {
            val layout = itemView.findViewById<CardView>(R.id.cardId)
            val imagen = itemView.findViewById<ImageView>(R.id.imgCarro)
            val fecha = itemView.findViewById<TextView>(R.id.citaD)
            val titulo = itemView.findViewById<TextView>(R.id.Cita)
            val horaI = itemView.findViewById<TextView>(R.id.horaInicio)
            val horaF = itemView.findViewById<TextView>(R.id.horaFin)

            Log.i("nuevo", "Request para Consultar ventas")
            val string =pelicula.get("fecha").toString()
            val tipos= pelicula.get("idClasificador").toString()
            fecha.setText(string)
            titulo.setText(pelicula.get("descripcion").toString())
            horaI.setText(pelicula.get("horaInicio").toString())
            horaF.setText(pelicula.get("horaFin").toString())
            layout.setOnClickListener {
                informacion(pelicula)
            }
            when(tipos){
                "1"-> Picasso.with(c)
                    .load(R.mipmap.salon)
                    .error(R.mipmap.peinado)
                    .into(imagen)
                "2"-> Picasso.with(c)
                    .load(R.mipmap.pelon)
                    .error(R.mipmap.peinado)
                    .into(imagen)
                "3"->Picasso.with(c)
                    .load(R.mipmap.cabello)
                    .error(R.mipmap.peinado)
                    .into(imagen)
                "4"-> Picasso.with(c)
                    .load(R.mipmap.nail)
                    .error(R.mipmap.peinado)
                    .into(imagen)
                else->
                    Picasso.with(c)
                        .load(R.mipmap.scissors)
                        .error(R.mipmap.peinado)
                        .into(imagen)
            }


        }

    }abstract  fun informacion(pelicula: JSONObject)
}


