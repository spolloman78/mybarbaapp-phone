package com.example.mybarbaapp_movil_master.ui.agendar

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.example.mybarbaapp_movil_master.AgendarCitaActivity
import com.example.mybarbaapp_movil_master.Clases.AdaptadorServicio
import com.example.mybarbaapp_movil_master.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.json.JSONArray
import org.json.JSONObject

class AgendarFragment : Fragment() {

    private lateinit var agendarViewModel: AgendarViewModel
    private lateinit var listaSeleccionados:JSONArray

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        agendarViewModel =
            ViewModelProvider(this).get(AgendarViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_gallery, container, false)

        agendarViewModel.text.observe(viewLifecycleOwner, Observer {

        })


        val lista=root.findViewById<RecyclerView>(R.id.ServiciosRe)
        val btn1=root.findViewById<ImageView>(R.id.imageButton)
        val btn2=root.findViewById<ImageView>(R.id.imageButton3)
        val btn3=root.findViewById<ImageView>(R.id.imageButton4)
        val btn4=root.findViewById<ImageView>(R.id.imageButton5)
        val btn5=root.findViewById<ImageView>(R.id.imageButton6)
        val btn6=root.findViewById<ImageView>(R.id.imageButton9)
        val btn7=root.findViewById<ImageView>(R.id.imageButton7)

        btn1.setOnClickListener{
            leerCategorias(root.context,lista,6)
        }
        btn2.setOnClickListener{
            leerCategorias(root.context,lista,5)
        }
        btn3.setOnClickListener{
            leerCategorias(root.context,lista,2)
        }
        btn4.setOnClickListener{
            leerCategorias(root.context,lista,1)
        }
        btn5.setOnClickListener{
            leerCategorias(root.context,lista,7)
        }
        btn6.setOnClickListener{
            leerCategorias(root.context,lista,4)
        }
        btn7.setOnClickListener{
            leerCategorias(root.context,lista,3)
        }

        leerServicios(root.context,lista)

        val fab: FloatingActionButton = root.findViewById(R.id.fab)
        fab.setOnClickListener { view ->

        }
        listaSeleccionados= JSONArray()
        return root
    }
    fun leerServicios(context:Context,recyclerView: RecyclerView){
        Log.i("nuevo", "Request para consultar Servicios ")
        val queue = Volley.newRequestQueue(context)
        val url = "http://192.168.2.35:3000/servicio"
        val json = JSONArray()
        val jsonObjectRequest = JsonArrayRequest(
                Request.Method.GET, url, json,
                Response.Listener { response ->
                    Log.i("Servicios", "Response ir $response")
                   respuestapasada(response,context,recyclerView)
                }, Response.ErrorListener { error ->
            error.printStackTrace()
            Log.i("nuevo", "Error bro"+error.printStackTrace())
        })
        queue.add(jsonObjectRequest)
    }
    fun leerCategorias(context: Context, recyclerView: RecyclerView, tipo:Int){
        if(tipo==0){
            leerServicios(context,recyclerView)
        }else{
            Log.i("nuevo", "Request para consultar Servicios ")
            val queue = Volley.newRequestQueue(context)
            val url = "http://192.168.2.35:3000/servicio/tipo/"+tipo
            val json = JSONArray()
            val jsonObjectRequest = JsonArrayRequest(
                    Request.Method.GET, url, json,
                    Response.Listener { response ->
                        Log.i("Servicios", "Response ir $response")
                        respuestapasada(response,context,recyclerView)
                    }, Response.ErrorListener { error ->
                error.printStackTrace()
                Log.i("nuevo", "Error bro"+error.printStackTrace())
            })
            queue.add(jsonObjectRequest)

        }

    }
    fun respuestapasada(jsonRespuesta: JSONArray,context: Context,recyclerView: RecyclerView){
        val pasada=recyclerView
        val tipo=0
        val manager = GridLayoutManager(context, 1)
        pasada.adapter=object : AdaptadorServicio(context, jsonRespuesta, R.layout.servicio_row,tipo){
            @RequiresApi(Build.VERSION_CODES.KITKAT)
            override fun Agregar(pelicula: JSONObject) {

                var m=0
                Log.i("entro","entreoxd"+listaSeleccionados.length())
                if(listaSeleccionados.length()==0){
                    listaSeleccionados.put(pelicula)
                }else{
                    Log.i("entro","entreoxd")
                for (numeros in 0 until  listaSeleccionados.length()) {
                    val item = listaSeleccionados.getJSONObject(numeros)
                    if(item.get("id").toString() == pelicula.get("id").toString()){
                        listaSeleccionados.remove(numeros)
                        Log.i("json",pelicula.get("descripcion").toString()+"Eliminado")
                    }else{
                        listaSeleccionados.put(pelicula)
                        Log.i("json",pelicula.get("descripcion").toString()+"Agregado")
                    }
                }}

                m=0
                val id= activity?.intent?.extras?.getInt("id",0)
                val intent = Intent(context, AgendarCitaActivity::class.java)
                Log.i("xd",lista.toString())
                intent.putExtra("id",id)
                Log.i("xd",id.toString())
                intent.putExtra("json",listaSeleccionados.toString())
                startActivity(intent)


            }

        }
        pasada?.layoutManager = manager

    }

    override fun onResume() {
        super.onResume()
        listaSeleccionados= JSONArray()
    }

}