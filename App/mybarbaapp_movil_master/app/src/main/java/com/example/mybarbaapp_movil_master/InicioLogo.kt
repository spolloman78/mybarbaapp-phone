package com.example.mybarbaapp_movil_master

import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView

class InicioLogo : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.inicio)
        Hilo().execute()
        val animationScale: Animation = AnimationUtils.loadAnimation(this, R.anim.shakeanimation)
        val boton = findViewById<ImageView>(R.id.imageViewLogo)
        boton.startAnimation(animationScale)
    }
    inner class Hilo: AsyncTask<Unit, Unit, Unit>(){


        override fun doInBackground(vararg p0: Unit?) {
            try{
                Thread.sleep(3800)
            }catch (e:Exception){
                e.printStackTrace()
            }
        }

        override fun onPostExecute(result: Unit?) {
            super.onPostExecute(result)
            val intent = Intent(this@InicioLogo,Login::class.java)
            startActivity(intent)
            finish()
        }

    }
}