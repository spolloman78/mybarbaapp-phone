package com.example.mybarbaapp_movil_master

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.*
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

//url = "http://192.168.2.35:3000/registrar/"
class Registrar : AppCompatActivity() {
    var URL_SERVIDOR = "http://192.168.2.35:3000/registrar.php?users"
    var etnombre: EditText? = null
    var etpaterno:EditText? = null
    var etmaterno:EditText? = null
    var etemail:EditText? = null
    var ettelefono:EditText? = null
    var etpass:EditText? = null
    var btnregistrar: Button? = null
    var ja: JSONArray? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.registro)
        etnombre = findViewById<EditText>(R.id.txtInputNombreRegistro)
        etpaterno = findViewById<EditText>(R.id.txtInputApellidoPaternoRegistro)
        etmaterno = findViewById<EditText>(R.id.txtInputApellidoMaternoRegistro)
        etemail = findViewById<EditText>(R.id.txtInputCorreoRegistro)
        ettelefono = findViewById<EditText>(R.id.txtInputTelefonoRegistro)
        etpass = findViewById<EditText>(R.id.txtInputContrasenaRegistro)
        btnregistrar = findViewById<Button>(R.id.btnEnviarRegistro)
        btnregistrar!!.setOnClickListener(View.OnClickListener {
            if(etnombre?.text.toString().length == 0|| etpaterno?.text.toString().length == 0
                || etmaterno?.text.toString().length == 0 || etemail?.text.toString().length == 0
                || ettelefono?.text.toString().length == 0 || etpass?.text.toString().length == 0){
                    "FAVOR DE LLENAR LOS CAMPOS".toast(this)
            }else{
            val jsond=JSONObject()
            jsond.put("nombre",etnombre?.text)
            jsond.put("aPaterno",etpaterno?.text)
            jsond.put("aMaterno",etmaterno?.text)
            jsond.put("email",etemail?.text)
            jsond.put("contra",etpass?.text)
            jsond.put("cumpleanos","1998-07-06")
            jsond.put("telefono",ettelefono?.text)
            registrar(this,jsond)}}
                )
    }

    fun registrar(context:Context,jsonObject: JSONObject) {
        Log.i("nuevo", "Request para Registrar Usuario")
        val queue = Volley.newRequestQueue(context)
        val url = "http://192.168.2.35:3000/users/add"
        val json = JSONObject()
        val jsonObjectRequest=JsonObjectRequest(Request.Method.POST,url,jsonObject,
            Response.Listener {response ->
                Log.i("nuevo","Response ir $response")
                val mensaje=response.get("resp").toString()
                if(response.get("tipo").toString()=="1"){
                    mensaje.toast(context)
                    val intent= Intent(this,MainActivity::class.java)
                    intent.putExtra("id",response.get("idUsuario").toString().toInt())
                    startActivity(intent)
                    finish()
                }else{
                    mensaje.toast(context)
                }

            }, Response.ErrorListener { error ->
                error.printStackTrace()
                Log.i("nuevo","Error bro al agregar el usuario")
            }
        )
        queue.add(jsonObjectRequest)
    }
    fun String.toast(c:Context){
        Toast.makeText(c,this,Toast.LENGTH_LONG).show()
    }
}