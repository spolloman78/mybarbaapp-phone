package com.example.mybarbaapp_movil_master.Clases

import android.content.Context
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.example.mybarbaapp_movil_master.R
import org.json.JSONArray
import org.json.JSONObject

 abstract class AdaptadorB (val c: Context, val lista: JSONArray, val res:Int, val tipo:Int):
    RecyclerView.Adapter<AdaptadorB.MiVH>() {
     var empleadoSele= JSONArray()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdaptadorB.MiVH {
        val view = LayoutInflater.from(c).inflate(res, null)
        return MiVH(view)
    }

    override fun getItemCount(): Int {
        return lista.length()
    }

    override fun onBindViewHolder(holder: AdaptadorB.MiVH, position: Int) {
        val json4 = lista.getJSONObject(position)
        holder.bind(json4,c)
    }


 inner class MiVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var promos= ArrayList<String>()
        var empleados= ArrayList<Int>()

        var idEmpleado=0
        fun bind(pelicula: JSONObject,context: Context) {
            val precio = itemView.findViewById<Spinner>(R.id.spinner)
            val titulo= itemView.findViewById<TextView>(R.id.titulop)
            Log.i("nuevo", "Request para Barberos")
            Log.i("nuevo", "Request para consultar Servicios ")
            var id= pelicula.get("id").toString()
            val queue = Volley.newRequestQueue(context)
            val url = "http://192.168.2.35:3000/servicio/"+id
            val json = JSONArray()
            val jsonObjectRequest = JsonArrayRequest(
                Request.Method.GET, url, json,
                Response.Listener { response ->
                    Log.i("Servicios", "Response ir $response")
                        for (numeros in 0 until  response.length()) {
                            val jsonob= response.getJSONObject(numeros)
                            promos.add(jsonob.get("nombre").toString()+" "+jsonob.get("aPaterno").toString()+" "+jsonob.get("aMaterno").toString())
                            empleados.add(jsonob.get("id").toString().toInt())
                        }
                    spinner(precio,context,pelicula)
                }, Response.ErrorListener { error ->
                    error.printStackTrace()
                    Log.i("nuevo", "Error bro"+error.printStackTrace())
                })
            queue.add(jsonObjectRequest)
            titulo.setText(pelicula.get("servicio").toString())
        }
        fun spinner(precio: Spinner,context: Context,pelicula: JSONObject){
            Log.i("spinner",promos.toString())
            val adapter = ArrayAdapter(context,
                android.R.layout.simple_spinner_item, promos)
            adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
            precio.adapter=adapter
            precio.onItemSelectedListener= object :AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    TODO("Not yet implemented")
                }

                @RequiresApi(Build.VERSION_CODES.KITKAT)
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    Log.i("Seleccionado","Seleccionado"+empleadoSele.length())
                    if(empleadoSele.length()==0){
                        val jsn=JSONObject()
                        jsn.put("idServicio",pelicula.get("id").toString())
                        jsn.put("idEmpleado",empleados.get(position))
                        empleadoSele.put(jsn)
                    }else{
                    for (numeros in 0 until  empleadoSele.length()) {
                        val jsons=empleadoSele.getJSONObject(numeros)
                        if(jsons.get("idServicio").toString()==pelicula.get("id").toString()){
                            empleadoSele.remove(numeros)
                            val jsn=JSONObject()
                            jsn.put("idServicio",pelicula.get("id").toString())
                            jsn.put("idEmpleado",empleados.get(position))
                            empleadoSele.put(jsn)
                        }else{
                            val jsn=JSONObject()
                            jsn.put("idServicio",pelicula.get("id").toString())
                            jsn.put("idEmpleado",empleados.get(position))
                            empleadoSele.put(jsn)
                        }}
                    seleccionar(empleadoSele)
                }

            }
        }}

    }abstract fun seleccionar(lista:JSONArray)

}


