package com.example.mybarbaapp_movil_master.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "NOMBRE DE USUARIO"

    }
    val text: LiveData<String> = _text
}