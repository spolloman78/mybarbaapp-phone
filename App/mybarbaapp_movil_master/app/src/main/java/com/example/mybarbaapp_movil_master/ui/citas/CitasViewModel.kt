package com.example.mybarbaapp_movil_master.ui.citas


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CitasViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "CITAS"
    }
    val text: LiveData<String> = _text
}