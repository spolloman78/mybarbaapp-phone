package com.example.mybarbaapp_movil_master.ui.promocion

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.mybarbaapp_movil_master.R
import com.synnapps.carouselview.CarouselView
import com.synnapps.carouselview.ImageListener


class PromoFragment : Fragment() {

    private lateinit var promoViewModel: PromoViewModel
    var sampleImages = intArrayOf(
        R.mipmap.home,
        R.mipmap.the_cave_masaje,
        R.mipmap.the_cave_mascarilla,
        R.mipmap.the_cave_combo_corte_barba
    )
    var promos= ArrayList<String>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        promoViewModel =
            ViewModelProvider(this).get(PromoViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_promo, container, false)
        var textView: TextView = root.findViewById(R.id.text_promo)
        promoViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        promos.add("Cumplañero al 100")
        promos.add("Enero Fabuloso 50% de Descuento en tu segunda Barba")
        promos.add("Cumplañero al 100")
        promos.add("Febrero Amoroso 20% de Descuento en tu segunda Pareja")
        val carouselView =root.findViewById(R.id.carouselView2) as CarouselView;
        carouselView?.setPageCount(sampleImages.size);
        carouselView?.setImageListener(imageListener);

        val listView = root.findViewById<ListView>(R.id.lista)
        val adapter = ArrayAdapter(root.context, android.R.layout.simple_list_item_1, promos)
        listView.setAdapter(adapter)
        return root
    }
    var imageListener: ImageListener = object : ImageListener {
        override fun setImageForPosition(position: Int, imageView: ImageView) {
            // You can use Glide or Picasso here

            imageView.setImageResource(sampleImages[position]);
        }}
}