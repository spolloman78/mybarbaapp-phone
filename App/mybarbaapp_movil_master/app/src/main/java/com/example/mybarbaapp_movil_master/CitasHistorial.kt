package com.example.mybarbaapp_movil_master

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.example.mybarbaapp_movil_master.Clases.Adaptador
import com.example.mybarbaapp_movil_master.ui.main.SectionsPagerAdapter
import org.json.JSONArray
import org.json.JSONObject

class CitasHistorial() : AppCompatActivity() {

    private val pendingIntent: PendingIntent? = null
    private val CHANNEL_ID = "NOTIFICACION"
    private var id:Int = 0
    private val NOTIFICACION_ID = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_citas_historial)
        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        id= intent.extras?.getInt("id",0)!!
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
        val fab: FloatingActionButton = findViewById(R.id.fab)
        val adapter = ViewPayerAdapter(supportFragmentManager)
        adapter.addFragment(citasFragment(), "Proximas Citas")
        adapter.addFragment(HistorialFragment(), "Citas Pasadas")
        leer()
        leerPasadas()
        createNotificationChannel()
        setAlarm(this)
        viewPager.adapter = adapter
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    fun leer() :JSONArray{
        Log.i("nuevo", "Request para Consultar ventas")
        val queue = Volley.newRequestQueue(this)
        val url = "http://192.168.2.35:3000/citas/"+id
        val json = JSONArray()
        val jsonObjectRequest = JsonArrayRequest(
            Request.Method.GET, url, json,
            Response.Listener { response ->
                Log.i("nuevo", "Response ir $response")
                respuesta(response)
            }, Response.ErrorListener { error ->
                error.printStackTrace()
                Log.i("nuevo", "Error bro")
            })
        queue.add(jsonObjectRequest)
        return  json
    }
    // Iniciamos alarma de notificaciones
    fun setAlarm(context: Context) {
        val mgr = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        //configuramos una alarma para que se haga el envio de las notificaciones sino esta creada ya
        val alarmUp = PendingIntent.getBroadcast(context, 0,
            Intent(context, MyReceiver::class.java), PendingIntent.FLAG_NO_CREATE) != null
        if (!alarmUp) {
            Log.i("alarma","se puede")
            val intent = Intent(this, MyReceiver::class.java)
            intent.putExtra("id",id)
            val pIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            mgr.setRepeating(
                AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + 60,120,
                pIntent)
        } else {
            Log.i("alarma","Ya existente")

        }
    }


    override fun onResume() {
        super.onResume()
        leer()
        leerPasadas()
    }
    // Creamos canal para notificaciones
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = "Noticacion"
            val notificationChannel = NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT)
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }
    fun leerPasadas(){
        Log.i("nuevo", "Request para Consultar ventas")
        val queue = Volley.newRequestQueue(this)
        val url = "http://192.168.2.35:3000/citas/Pasadas/"+id
        val json = JSONArray()
        val jsonObjectRequest = JsonArrayRequest(
            Request.Method.GET, url, json,
            Response.Listener { response ->
                Log.i("citasPasadas", "Response ir $response")
                respuestapasada(response)
            }, Response.ErrorListener { error ->
                error.printStackTrace()
                Log.i("nuevo", "Error bro"+error.printStackTrace())
            })
        queue.add(jsonObjectRequest)
    }


    fun respuesta(jsonRespuesta: JSONArray){
        val citas= findViewById<RecyclerView>(R.id.citasF)
        val tipo=0
        val manager = GridLayoutManager(this, 1)
        citas.adapter=object :Adaptador(this@CitasHistorial, jsonRespuesta, R.layout.cita_row,tipo){
            override fun informacion(pelicula: JSONObject) {
                val intent = Intent(this@CitasHistorial,DetallesCita::class.java)
                intent.putExtra("json",pelicula.toString())
                startActivity(intent)
            }

        }
        citas?.layoutManager = manager
    }
    fun respuestapasada(jsonRespuesta: JSONArray){
        val pasada= findViewById<RecyclerView>(R.id.citasP)
        val tipo=0
        val manager = GridLayoutManager(this, 1)
        pasada.adapter=object :Adaptador(this@CitasHistorial, jsonRespuesta, R.layout.cita_row,tipo){
            override fun informacion(pelicula: JSONObject) {
                val intent = Intent(this@CitasHistorial,DetallesCita::class.java)
                intent.putExtra("json",pelicula.toString())
                startActivity(intent)
            }

        }
        pasada?.layoutManager = manager

    }

}