package com.example.mybarbaapp_movil_master.Clases

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.mybarbaapp_movil_master.R
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONObject

abstract class AdaptadorServicio (val c: Context?, val lista: JSONArray, val res:Int, val tipo:Int):
            RecyclerView.Adapter<AdaptadorServicio.MiVH>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdaptadorServicio.MiVH {
            val view = LayoutInflater.from(c).inflate(res, null)
            return MiVH(view)
        }

        override fun getItemCount(): Int {
            return lista.length()
        }

        override fun onBindViewHolder(holder: AdaptadorServicio.MiVH, position: Int) {
            val json4 = lista.getJSONObject(position)
            holder.bind(json4)
        }
        inner class MiVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bind(pelicula: JSONObject) {

                val layout = itemView.findViewById<CardView>(R.id.cardD2)
                val precio = itemView.findViewById<TextView>(R.id.precioD)
                val titulo= itemView.findViewById<TextView>(R.id.titulo)
                val enca= itemView.findViewById<TextView>(R.id.header)
                val duracion = itemView.findViewById<TextView>(R.id.duracion)

                Log.i("nuevo", "Request para Servicios")

                titulo.setText(pelicula.get("servicio").toString())
                precio.setText("$ "+pelicula.get("precio").toString())
                duracion.setText(" "+pelicula.get("tiempo").toString())
                enca.setText("Servicios de "+pelicula.get("descripcion").toString())
                layout.setOnClickListener {
                    Agregar(pelicula)
                }



            }

        }abstract  fun Agregar(pelicula: JSONObject)
    }


