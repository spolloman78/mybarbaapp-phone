package com.example.mybarbaapp_movil_master





import android.accounts.AccountManager.get
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.PointerIcon.load
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionBarPolicy.get
import androidx.appcompat.widget.ResourceManagerInternal.get
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import androidx.fragment.app.replace
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.mybarbaapp_movil_master.ui.agendar.AgendarFragment
import com.example.mybarbaapp_movil_master.ui.gallery.GalleryFragment
import com.example.mybarbaapp_movil_master.ui.home.HomeFragment
import com.example.mybarbaapp_movil_master.ui.promocion.PromoFragment
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import com.synnapps.carouselview.CarouselView
import com.synnapps.carouselview.ImageListener
import java.lang.System.load
import kotlin.properties.Delegates


class MainActivity : AppCompatActivity() {
    private lateinit var appBarConfiguration: AppBarConfiguration
    var id:Int = 0
    var sampleImages = arrayOf(
        "https://raw.githubusercontent.com/sayyam/carouselview/master/sample/src/main/res/drawable/image_3.jpg",
        "https://raw.githubusercontent.com/sayyam/carouselview/master/sample/src/main/res/drawable/image_1.jpg",
        "https://raw.githubusercontent.com/sayyam/carouselview/master/sample/src/main/res/drawable/image_2.jpg"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        id= intent.extras?.getInt("id",0)!!
        

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        appBarConfiguration = AppBarConfiguration(setOf(
        R.id.nav_agendar, R.id.nav_home, R.id.nav_promo), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        setupNavigationDrawerContent(navView);



        }



        override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
        }



        fun cambiar(){
            val intent = Intent(this@MainActivity,CitasHistorial::class.java)
            intent.putExtra("id",id)
            startActivity(intent)
            Log.i("jeje","xd")
        }
    fun setFragment(position:Int)
    {
        when(position) {
            1->{
                val fragment= HomeFragment()
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.nav_host_fragment, fragment)
                transaction.addToBackStack(null)
                transaction.commit()}
            2->{
                val fragment= PromoFragment()
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.nav_host_fragment,fragment)
                transaction.addToBackStack(null)
                transaction.commit()
            }
            3->{
                val fragment= AgendarFragment()
                val transaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.nav_host_fragment, fragment)
                transaction.addToBackStack(null)
                transaction.commit()
            }
            4-> cambiar()
            5->cerrar()

        }

    }
    fun cerrar(){
        val intent = Intent(this@MainActivity,Login::class.java)
        startActivity(intent)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
            val navController = findNavController(R.id.nav_host_fragment)
            return navController.navigateUp(appBarConfiguration)
        return super.onOptionsItemSelected(item)
    }
    private fun setupNavigationDrawerContent(navigationView: NavigationView) {
        navigationView.setNavigationItemSelectedListener(
            NavigationView.OnNavigationItemSelectedListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.nav_home-> {
                        menuItem.isChecked = true
                        setFragment(1)
                        return@OnNavigationItemSelectedListener true
                    }
                    R.id.nav_promo -> {
                        menuItem.isChecked = true
                        setFragment(2)
                        return@OnNavigationItemSelectedListener true
                    }
                    R.id.nav_agendar -> {
                        menuItem.isChecked = true

                        setFragment(3)
                        return@OnNavigationItemSelectedListener true
                    }
                    R.id.nav_citas -> {
                        menuItem.isChecked = true

                        setFragment(4)
                        return@OnNavigationItemSelectedListener true
                    }
                    R.id.cerrar->{
                        menuItem.isChecked = true
                        setFragment(5)

                    }

                }
                true
            })
    }


}
