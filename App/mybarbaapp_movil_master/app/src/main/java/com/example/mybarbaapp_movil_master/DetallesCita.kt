package com.example.mybarbaapp_movil_master

import android.os.Bundle
import android.widget.TextView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import org.json.JSONObject

class DetallesCita : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_detalles_cita)
            val json2=intent.extras?.getString("json","{}")
            var json= JSONObject(json2)
            val servicio= findViewById<TextView>(R.id.Servicio)
            servicio.setText(json.get("descripcion").toString())
            val nombre =findViewById<TextView>(R.id.textView3)
            nombre.setText(json.get("nombre").toString())
            val fecha=findViewById<TextView>(R.id.cantidadauto)
            fecha.setText(json.get("fecha").toString()+" ")
            val detalles= findViewById<TextView>(R.id.precio2)
            detalles.setText("Horario: "+json.get("horaInicio").toString()+"-"+json.get("horaFin").toString()+"\n\nNumero de Visitas: "+json.get("numeroVisitas").toString())
            val estado= findViewById<TextView>(R.id.Visitas)
            estado.setText("Estado: "+json.get("estado").toString())

            findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            }
        }

}