package com.example.mybarbaapp_movil_master.ui.agendar

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AgendarViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "AGENDAR"
    }
    val text: LiveData<String> = _text
}