package com.example.mybarbaapp_movil_master.ui.promocion

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PromoViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "PROMOCIONES"
    }
    val text: LiveData<String> = _text
}