package com.example.mybarbaapp_movil_master

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class Login : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)
        val boton= findViewById<Button>(R.id.bot)
        val botonIngresar= findViewById<Button>(R.id.login)
        val usuarios=findViewById<EditText>(R.id.email)
        val contra=findViewById<EditText>(R.id.contra)

        boton.setOnClickListener {
            val intent= Intent(this,Registrar::class.java)
            startActivity(intent)
        }
        botonIngresar.setOnClickListener {
            if (usuarios?.text.toString().length == 0 || contra?.text.toString().length == 0) {
                "FAVOR DE LLENAR LOS CAMPOS".toast(this)
            } else {
                val usuario = JSONObject()
                usuario.put("email", usuarios?.text)
                usuario.put("contra", contra?.text)
                login(this, usuario)
                contra?.setText("")
                usuarios?.setText("")
            }
        }
    }
    fun login(context: Context, jsonObject: JSONObject) {
        Log.i("nuevo", "Request logearse")
        val queue = Volley.newRequestQueue(context)
        val url = "http://192.168.2.35:3000/users/"
        val json = JSONObject()
        val jsonObjectRequest= JsonObjectRequest(Request.Method.POST,url,jsonObject,
            Response.Listener { response ->
                Log.i("nuevo","Response ir $response")
                val mensaje=response.get("resp").toString()
                if(response.get("tipo").toString()=="1"){
                    mensaje.toast(context)
                    val intent= Intent(this,MainActivity::class.java)
                    intent.putExtra("id",response.get("idUsuario").toString().toInt())
                    startActivity(intent)
                }else{
                    """Los datos introducidos no coinciden, 
                       Favor de intentarlo de nuevo""".toast(context)
                }

            }, Response.ErrorListener { error ->
                error.printStackTrace()
               """Problemas para establecer la conexión con el servidor
                       Favor de intentar más tarde""".toast(context)
            }
        )
        queue.add(jsonObjectRequest)
    }
    fun String.toast(c: Context){
        Toast.makeText(c,this, Toast.LENGTH_LONG).show()
    }
}