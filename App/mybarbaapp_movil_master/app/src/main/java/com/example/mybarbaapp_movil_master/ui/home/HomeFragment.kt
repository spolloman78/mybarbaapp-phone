package com.example.mybarbaapp_movil_master.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.mybarbaapp_movil_master.R
import com.synnapps.carouselview.CarouselView
import com.synnapps.carouselview.ImageListener

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    var sampleImages = intArrayOf(
        R.mipmap.home,
        R.mipmap.the_cave_logo,
        R.mipmap.the_cave_barba,
        R.mipmap.the_cave_certificados
    )
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
                ViewModelProvider(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        homeViewModel.text.observe(viewLifecycleOwner, Observer {

        })
        val carouselView =root.findViewById(R.id.carouselView) as CarouselView;
        carouselView?.setPageCount(sampleImages.size);
        carouselView?.setImageListener(imageListener);
        return root
    }
    var imageListener: ImageListener = object : ImageListener {
        override fun setImageForPosition(position: Int, imageView: ImageView) {
            // You can use Glide or Picasso here

            imageView.setImageResource(sampleImages[position]);
        }}
}