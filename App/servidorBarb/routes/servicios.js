var express = require('express');
var router = express.Router();
const pool = require('../database');
router.get('/', async (req, res) => {
    const servicios = await pool.query('SELECT servicios.descripcion as servicio, servicios.id, servicios.tiempo, servicios.precio,clasificador_servicios.descripcion FROM servicios,clasificador_servicios where servicios.idClasificador= clasificador_servicios.id');
    res.json(servicios);
});
router.get('/tipo/:id', async (req, res) => {
      const { id } = req.params;
    const servicios = await pool.query('SELECT servicios.descripcion as servicio, servicios.id, servicios.tiempo, servicios.precio,clasificador_servicios.descripcion FROM servicios,clasificador_servicios where servicios.idClasificador= clasificador_servicios.id and clasificador_servicios.id= '+id);
    res.json(servicios);
});
router.get('/:id', async (req, res) => {
    const { id } = req.params;
    const servicios = await pool.query('SELECT empleados.id,empleados.nombre,empleados.aPaterno, empleados.aMaterno FROM empleados,empleados_servicios where empleados.id= empleados_servicios.idEmpleado and empleados_servicios.idServicio='+id+' GROUP BY(empleados.id)');
    res.json(servicios);
});

//SELECT empleados.nombre,empleados.aPaterno, empleados.aMaterno,servicios.descripcion as servicio, servicios.id, servicios.tiempo, servicios.precio FROM empleados,empleados_servicios,servicios,clasificador_servicios where servicios.idClasificador= clasificador_servicios.id and empleados.id= empleados_servicios.idEmpleado and servicios.id= empleados_servicios.idServicio


module.exports = router;
