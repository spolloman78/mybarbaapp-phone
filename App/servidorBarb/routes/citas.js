var express = require('express');
var router = express.Router();
const pool = require('../database');
var moment = require('moment'); // require
// moment.locale('es');
var hoy= new Date();
var DIA_EN_MILISEGUNDOS = 24 * 60 * 60 * 1000;
var ayer = new Date(hoy.getTime() +(DIA_EN_MILISEGUNDOS*2));
var tomorrow= new Date(hoy.getTime() - DIA_EN_MILISEGUNDOS);
var fechaTo =  ayer.getFullYear() + '-' + ( ayer.getMonth() + 1 ) + '-' +ayer.getDate();
var fecha =  hoy.getFullYear() + '-' + ( hoy.getMonth() + 1 ) + '-' +hoy.getDate();
router.get('/:id', async (req, res) => {
    const { id } = req.params;
    const citas = await pool.query('SELECT Date_format(fecha,"%d/%m/%Y") as fecha,idClasificador,estados_citas.descripcion as estado,idEmpleado,numeroVisitas, horaInicio, horaFin,servicios.descripcion,concat(empleados.nombre," ", empleados.aPaterno," ", empleados.aMaterno) as nombre FROM empleados,citas,servicios,clientes,estados_citas where estados_citas.id=citas.idEstadoCita and  servicios.id = citas.idServicio and empleados.id =citas.idEmpleado and citas.idCliente=clientes.id and citas.fecha >="'+fecha+'" and idEstadoCita!=3 and idCliente='+id);
    console.log(citas);
    res.json(citas);
});
router.get('/Cercanas/:id', async (req, res) => {
    const { id } = req.params;
    const citas = await pool.query('SELECT Date_format(fecha,"%d/%m/%Y") as fecha,estados_citas.descripcion as estado,idEmpleado,numeroVisitas, horaInicio, horaFin,servicios.descripcion,concat(empleados.nombre," ", empleados.aPaterno," ", empleados.aMaterno) as nombre FROM empleados,citas,servicios,clientes,estados_citas where estados_citas.id=citas.idEstadoCita and  servicios.id = citas.idServicio and empleados.id =citas.idEmpleado and citas.idCliente=clientes.id and citas.fecha >="'+fecha+'"and citas.fecha <="'+fechaTo+'"and idCliente='+id);
    res.json(citas);
});
router.get('/Pasadas/:id', async (req, res) => {
    const { id } = req.params;
    const citas = await pool.query('SELECT Date_format(fecha,"%d/%m/%Y") as fecha,idClasificador,estados_citas.descripcion as estado,idEmpleado,numeroVisitas, horaInicio, horaFin,servicios.descripcion,concat(empleados.nombre," ", empleados.aPaterno," ", empleados.aMaterno) as nombre FROM empleados,citas,servicios,clientes,estados_citas where estados_citas.id=citas.idEstadoCita and servicios.id = citas.idServicio and empleados.id=citas.idEmpleado and citas.idCliente=clientes.id and citas.fecha <="'+fecha+'"and idEstadoCita!=3 and idCliente='+id);
    console.log(citas);
    res.json(citas);
});

router.post('/agendar', async (req, res) => {
    const {idServicio,idEmpleado,fechas,hora,idCliente,tiempo}=req.body;

    //let id_empleado = req.params.id_empleado; // Obtengo el id del Empleado
    //id_empleado = parseInt(id_empleado);
    console.log(fechas);
    let fecha =fechas; // Obtenemos la fecha de la cita
    const hora_Inicio = moment(hora,'HH:mm:ss'); // Obtenemos la hora de la cita
    let horaFin = hora_Inicio.clone(); // Realizamos una copia con clone para trabajar la hora final
    const duracion = tiempo; // Obtenemos la duracion de la cita
    horaFin.add(duracion); //Relizo la suma de la hora con la duracion
    horaFin = horaFin.format('HH:mm:ss'); // Le doy formato de hora
    console.log("Hora inicio:");
    let horaF=horaFin;
    console.log(hora_Inicio.format('H:mm:ss'));
    console.log("Hora fin:");
    console.log(horaFin);

    // return;horaFin.format('HH:mm:ss');

    const query = `select date_format(fecha,"%d-%m-%Y") as fecha, horaInicio, horaFin from citas
                    where date_format(fecha,"%d-%m-%Y") = '${fecha}' and
                    idEmpleado = ${idEmpleado} and
                    horaInicio <= '${hora}:00' and
                    horaFin >= '${horaF}' or
                    ( idEmpleado = ${idEmpleado} and horaInicio <= '${hora}:00' and horaFin <= '${horaF}' and horaInicio >='${hora}:00') or
                    ( idEmpleado = ${idEmpleado} and horaInicio >= '${hora}:00' and horaFin >= '${horaF}' and horaInicio <='${horaF}') or
                    ( idEmpleado = ${idEmpleado} and horaInicio <= '${hora}:00' and horaFin >= '${horaF}' ) `;
                    console.log(query);
    const citas = await pool.query(query);
    console.log(citas.length);

    if(citas.length > 0){
        const tipo=1;
        const re="Horario no disponible";
        const respuesta={
          re,tipo
        }
      return res.json(respuesta);
    }else{

        // Realizamos la insercion o damos mensaje de horario disponible
      const idEstadoCita=5;
      const id=0;

      const horaInicio=hora;
      const datos={id,idServicio,idEmpleado,fecha,horaInicio,horaFin,idCliente,idEstadoCita}
      console.log(datos);
      await pool.query("INSERT INTO citas set ?",[datos]);
      const re="Agregado con éxito";
      const tipo=2;
      const respuesta={
        re,tipo
      }
      return res.json(respuesta);
    }

  //  return res.json(citas);
});

module.exports = router;
