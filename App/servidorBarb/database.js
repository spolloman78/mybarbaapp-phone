var mysql = require('mysql');
const { promisify }= require('util');
var dbConfig = {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'cave',
    connectionLimit: 50,
    queueLimit: 0,
    waitForConnection: true
};
var pool= mysql.createPool(dbConfig);
pool.getConnection((err, connection) => {
  if (err) {
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.error('Database connection was closed.');
    }
    if (err.code === 'ER_CON_COUNT_ERROR') {
      console.error('Database has to many connections');
    }
    if (err.code === 'ECONNREFUSED') {
      console.error('Database connection was refused');
    }
  }
  if (connection) connection.release();
  console.log('DB is Connected');
  return;
});
// Promisify Pool Querys
pool.query = promisify(pool.query);
// Promisify Pool Querys
module.exports = pool;
